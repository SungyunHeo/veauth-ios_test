Pod::Spec.new do |s|
  s.name         = "VeAuth"
  s.version      = "0.0.10"
  s.summary      = "Veaver Auth Framework"
  s.description  = "Veaver Auth Framework"
  s.homepage     = "http://www.veaver.com/VeAuth"
  s.license      = "Copyright Veaver ENT"
  s.author             = { "허성연" => "syheo@veaver.com" }
  s.ios.deployment_target = '10.0'
  s.ios.vendored_frameworks = 'VeAuth.framework'
  s.source = { :git => 'https://SungyunHeo@bitbucket.org/SungyunHeo/veauth-ios_test.git', :tag => s.version }
  s.exclude_files = "Classes/Exclude"
  s.dependency "Alamofire"
end
